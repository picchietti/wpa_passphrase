use clap::Parser;
use wpa_passphrase::generate_psk_sha1;

fn exit_with_error(message: &str) {
    eprintln!("{}", message);
    std::process::exit(1);
}

fn read_passphrase() -> String {
    println!("# reading passphrase from stdin");
    let mut passphrase = String::new();
    let stdin = std::io::stdin();
    stdin.read_line(&mut passphrase).unwrap();
    passphrase.pop(); // remove trailing newline
    passphrase
}

fn validate_passphrase(passphrase: &str) {
    let passphrase_length = passphrase.chars().count();
    if !(8..64).contains(&passphrase_length) {
        exit_with_error("Passphrase must be 8..63 characters");
    }

    if passphrase.chars().any(|i| char::is_ascii_control(&i)) {
        exit_with_error("Invalid passphrase character");
    }
}

/// Computes PSK (Pre-Shared Key) entries for network configuration blocks of a wpa_supplicant.conf file. A SSID and ASCII passphrase are used to generate a 256-bit PSK.
#[derive(Parser, Debug)]
#[clap(version)]
struct Arguments {
    /// The SSID whose passphrase should be derived
    ssid: String,
    /// The passphrase to use. If not included on the command line, passphrase will be read from standard input
    passphrase: Option<String>,
}

fn main() {
    let args = Arguments::parse();
    let ssid = args.ssid;
    let passphrase = args.passphrase.unwrap_or_else(read_passphrase);
    validate_passphrase(&passphrase);
    let psk = generate_psk_sha1(&ssid, &passphrase);

    println!(
        "network={{
\tssid=\"{}\"
\t#psk=\"{}\"
\tpsk={}
}}",
        ssid, passphrase, psk
    );
}

use hmac::Hmac;
use pbkdf2::pbkdf2;
use sha1::Sha1;
use std::fmt::Write;

// TODO Change the signature to return Result<String> now that pbkdf2 uses Result.
pub fn generate_psk_sha1(ssid: &String, passphrase: &String) -> String {
    let mut byte_psk = [0u8; 32];
    pbkdf2::<Hmac<Sha1>>(passphrase.as_bytes(), ssid.as_bytes(), 4096, &mut byte_psk)
        .expect("Problem generating byte psk");
    let hex_psk: String = byte_psk.iter().fold(String::new(), |mut output, b| {
        let _ = write!(output, "{b:02x}");
        output
    });
    hex_psk
}

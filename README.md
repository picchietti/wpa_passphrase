# wpa_passphrase

Generate a Wpa PSK (Pre-Shared Key) from a ssid and passphrase.

This is rust rewrite of the [wpa_passphrase](https://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_passphrase.c) program written for wpa_supplicant. See the [README](https://w1.fi/cgit/hostap/plain/wpa_supplicant/README) for license information.

[Install the binary](https://doc.rust-lang.org/book/ch14-04-installing-binaries.html) with `cargo install wpa_passphrase`.

In addition to the binary, this crate is a library that provides a `generate_psk_sha1` function for use in other rust programs.
